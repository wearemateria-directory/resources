# WeAreMateria&nbsp;&nbsp;|&nbsp;&nbsp;Directory



## Recursos

- [Wordpress](#wordpress)


___
## Wordpress

- [Temas](#temas)

- [Plugins](#plugins)


___
## Temas

- [WooCommerce](#woocommerce)

- [Eventos](#eventos)

- [Logística](#logistica)

- [Job Board](#job-board)

- [Multi-Purpose](#multi-purpose)

- [One-Page](#one-page)


___
## Plugins

- _TODO_


___
## WooCommerce

- [Bazaar](http://bazaar.select-themes.com/landing) - A Modern, Sharp eCommerce Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/b31332d6-8213-4dec-a6cf-165648893c9b)

- [Upscale](http://dahz.daffyhazan.com/upscale/) - Fashionable eCommerce WordPress Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/febfc658-4074-4463-ad22-9c3a33753f2d)

- [Oasis](http://oasis.la-studioweb.com/landing) - Modern WooCommerce Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/9764332b-cfa0-482d-b204-4f05435aa6ab)

- [Basel](https://demo.xtemos.com/basel/) - Responsive eCommerce Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/0c6e6045-4d62-4630-af16-36c12f8be660)

- [Noren](http://noren.themestudio.net/) - Shop WordPress WooCommerce Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/ead3b936-ae98-4478-a7fe-b13e50e9f019)

- [Sober](http://uix.store/intro/sober/) - Premium WordPress Shopping Theme | [Download alternativo]

- [Milano](http://milano.leetheme.com/demo/) - Responsive Ecommerce Wordpress Theme | [Download alternativo]

- [XStore](https://8theme.com/demo/xstore/preview-new/) - Responsive WooCommerce Theme | [Download alternativo]


___
## Eventos

- [Evently](http://evently.mikado-themes.com/landing/) - A Modern Multi-Concept Event and Conference Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/80347866-6067-4d9a-a981-23b34cc69dc3)


___
## Logística

- [TruckPress](http://demo2.steelthemes.com/truckpress/) - Warehouse, Logistics & Transportation WP Theme | [Download alternativo]


___
## Job Board

- [Jobplanet](http://jobplanet.jegtheme.com/) -
Responsive Job Board WordPress Themes | [Download](https://themeforest.net/user/greymateria/download_purchase/bc988b71-aadc-402b-bb19-fd300b3b2690)


___
## Multi-purpose

- [Boo](http://boo.themerella.com/boo-landing/index.html) - Responsive Multi-Purpose WordPress Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/c0286613-f9e0-44b4-a88e-c1c2a9d47022)

- [Stack](http://trystack.mediumra.re/) - Multi-Purpose HTML with Page Builder | [Download](https://themeforest.net/user/greymateria/download_purchase/8a8ede03-6340-49f4-9d68-ff222b181937)

- [deKor](http://demo.templaza.com/dekor-html/?tf=1) - Responsive Interior HTML Template | [Download](https://themeforest.net/user/greymateria/download_purchase/4456d721-b34a-4fc6-9cb0-e15540e8732a)

- [Chariot](http://demo.az-themes.com/chariot/) - Professional Responsive Portfolio Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/ae0e7dd2-9574-4f84-b749-db12f428db47)


___
## One-page

- [IGNITE](http://designova.net/themes/wordpress/ignite/) - Simple One Page Creative WordPress Theme | [Download](https://themeforest.net/user/greymateria/download_purchase/8b2e143a-2042-463c-9adc-4b7227fec4eb)
